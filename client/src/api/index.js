import axios from "axios";

const API = axios.create({
  baseURL: "http://localhost:5000",
});

const TOKEN = JSON.parse(localStorage.getItem("profile"))?.accessToken;

// USERS
export const login = (formData) => API.post("/auth/login", formData);
export const register = (formData) =>
  API.post("/auth/register", formData, {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });
export const updateUser = (id, formData) =>
  API.patch(`/auth/${id}`, formData, {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });
export const deleteUser = (id) =>
  API.delete(`/auth/${id}`, {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });
export const getUsers = () =>
  API.get("/auth/users", {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });
export const getUser = (id) =>
  API.get(`/auth/${id}`, {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });

// MEMBERS
export const addMember = (formData) =>
  API.post("/member/add", {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });

export const updateMember = (id, formData) =>
  API.patch(`/member/${id}`, formData, {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });
export const deleteMember = (id) =>
  API.delete(`/member/${id}`, {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });

export const getMember = (id) =>
  API.get(`/member/${id}`, {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });

export const getMembers = () =>
  API.get("/member/", {
    headers: {
      token: `Bearer ${TOKEN}`,
    },
  });
