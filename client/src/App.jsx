import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AddUser from "./pages/AddUser";
import Home from "./pages/Home";
import Login from "./pages/Login";
import AddMember from "./pages/AddMember";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route index exact path="/" element={<Login />} />
        <Route path="/addUser" element={<AddUser />} />
        <Route path="/home" element={<Home />} />
        <Route path="/addMember" element={<AddMember />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
