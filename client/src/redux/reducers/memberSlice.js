import { createSlice } from "@reduxjs/toolkit";
export const memberSlice = createSlice({
  name: "member",
  initialState: {
    pending: false,
    error: false,
    members: [],
    member: {},
  },

  reducers: {
    START_LOADING: (state) => {
      state.pending = true;
    },
    END_LOADING: (state) => {
      state.pending = false;
    },
    FETCH_MEMBERS: (state, action) => {
      state.members = action.payload;
    },
    FETCH_MEMBER: (state, action) => {
      state.member = action.payload;
    },
  },
});

export const { START_LOADING, END_LOADING, FETCH_MEMBER, FETCH_MEMBERS } =
  memberSlice.actions;

export default memberSlice.reducer;
