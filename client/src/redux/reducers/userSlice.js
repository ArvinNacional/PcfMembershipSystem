import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    userInfo: null,
    pending: false,
    error: false,
  },

  reducers: {
    REGISTER: (state, action) => {},
    LOGIN: (state, action) => {
      localStorage.setItem("profile", JSON.stringify({ ...action?.payload }));
      state.userInfo = action?.payload;
    },
    LOG_OUT: (state) => {
      localStorage.clear();
      return {
        ...state,
        userInfo: null,
      };
    },
    ERROR: (state, action) => {},
    START_LOADING: (state) => {
      state.pending = true;
    },
    END_LOADING: (state) => {
      state.pending = false;
    },
  },
});

export const { REGISTER, LOGIN, ERROR, START_LOADING, END_LOADING } =
  userSlice.actions;

export default userSlice.reducer;
