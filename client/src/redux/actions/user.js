import * as api from "../../api/index";

import {
  REGISTER,
  LOGIN,
  START_LOADING,
  END_LOADING,
  ERROR,
} from "../reducers/userSlice";

export const register = (formData) => async (dispatch) => {
  try {
    console.log(formData);
    dispatch(START_LOADING());
    const { data } = await api.register(formData);
    dispatch(REGISTER(data));
    dispatch(END_LOADING());
  } catch (error) {
    dispatch(ERROR(error.response.data));
  }
};

export const login = (formData, navigate) => async (dispatch) => {
  try {
    console.log(formData);
    dispatch(START_LOADING());
    const { data } = await api.login(formData);
    dispatch(LOGIN(data));
    dispatch(END_LOADING());
    console.log(data);
    navigate("/home");
  } catch (error) {
    dispatch(ERROR(error.response.data));
  }
};
