import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./reducers/userSlice";
import memberSlice from "./reducers/memberSlice";

export default configureStore({
  reducer: {
    user: userSlice,
    member: memberSlice,
  },
});
