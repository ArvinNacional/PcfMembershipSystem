import jwt from "jsonwebtoken";
import User from "../models/User.js";
import CryptoJS from "crypto-js";
import dotenv from "dotenv";
dotenv.config();

// REGISTER
export const register = async (req, res) => {
  const { username, password, firstName, lastName, confirmPassword } = req.body;

  try {
    const existingUsername = await User.findOne({ username });
    if (existingUsername)
      return res.status(400).json({ message: "Username already exists." });
    if (password !== confirmPassword)
      return res.status(400).json({
        message: "Passwords don't match",
      });

    const newUser = new User({
      username,
      password: CryptoJS.AES.encrypt(
        password,
        process.env.PASSWORD_SECRET
      ).toString(),
      name: `${firstName} ${lastName}`,
    });
    const savedUser = await newUser.save();
    res.status(200).json(savedUser);
  } catch (error) {
    res.status(500).json(error.message);
  }
};

// LOGIN
export const login = async (req, res) => {
  try {
    const user = await User.findOne({
      username: req.body.username,
    });

    !user && res.status(401).json("User doesn't exist.");
    const hashedPassword = CryptoJS.AES.decrypt(
      user.password,
      process.env.PASSWORD_SECRET
    );
    const originalPassword = hashedPassword.toString(CryptoJS.enc.Utf8);
    originalPassword !== req.body.password &&
      res.status(401).json("Wrong password");

    const accessToken = jwt.sign(
      {
        id: user._id,
        isAdmin: user.isAdmin,
      },
      process.env.JWT_SECRET,
      { expiresIn: "3d" }
    );

    const { password, ...others } = user._doc;
    res.status(200).json({ ...others, accessToken });
  } catch (error) {
    res.status(500).json(error.message);
  }
};

// UPDATE
export const update = async (req, res) => {
  if (req.body.password === req.body.confirmPassword) {
    req.body.password = CryptoJS.AES.encrypt(
      req.body.password,
      process.env.PASSWORD_SECRET
    ).toString();
  }

  const { username, firstName, lastName } = req.body;

  let password = req.body.password;

  const user = {
    username,
    password,
    name: `${firstName} ${lastName}`,
  };

  try {
    const updatedUser = await User.findByIdAndUpdate(
      req.params.id,
      {
        $set: user,
      },
      {
        new: true,
      }
    );
    res.status(200).json(updatedUser);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// DELETE

export const remove = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.params.id);
    res.status(200).json("User has been deleted");
  } catch (error) {
    res.status(500).json(error.message);
  }
};

// GET ALL USERS

export const getAllUsers = async (req, res) => {
  const query = req.query.new;

  try {
    const users = query
      ? await User.find().limit(5).sort({ _id: -1 })
      : await User.find();

    res.status(200).json(users);
  } catch (error) {
    res.status(500).json(error.message);
  }
};

// GET USER

export const getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    const { password, ...others } = user._doc;
    res.status(200).json(others);
  } catch (error) {
    res.status(500).json(error.message);
  }
};
