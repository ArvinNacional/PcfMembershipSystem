import Member from "../models/Member.js";

// ADD MEMBER
export const addMember = async (req, res) => {
  const newMember = new Member(req.body);
  try {
    const savedMember = await newMember.save();
    res.status(200).json(savedMember);
  } catch (error) {
    res.status(500).json(error.message);
  }
};

// UPDATEMEMBER
export const updateMember = async (req, res) => {
  try {
    const updatedMember = await Member.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      {
        new: true,
      }
    );
    res.status(200).json(updatedMember);
  } catch (error) {
    res.status(500).json(error);
  }
};

// DELETE
export const removeMember = async (req, res) => {
  try {
    await Member.findByIdAndDelete(req.params.id);
    res.status(200).json("Member has been deleted.");
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getMember = async (req, res) => {
  try {
    const member = await Member.findById(req.params.id);
    res.status(200).json(member);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export const getMembers = async (req, res) => {
  const query = req.query.new;
  try {
    const members = query
      ? await Member.find().limit(5).sort({ _id: -1 })
      : await Member.find();
    res.status(200).json(members);
  } catch (error) {
    res.status(500).json(error.message);
  }
};
