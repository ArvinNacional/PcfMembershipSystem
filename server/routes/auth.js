import express from "express";
import {
  register,
  login,
  update,
  remove,
  getAllUsers,
  getUser,
} from "../controllers/auth.js";
import { verifyTokenAndAdmin } from "../controllers/verifyToken.js";
const router = express.Router();

router.post("/register", verifyTokenAndAdmin, register);
router.post("/login", login);
router.patch("/:id", verifyTokenAndAdmin, update);
router.delete("/:id", verifyTokenAndAdmin, remove);
router.get("/users", verifyTokenAndAdmin, getAllUsers);
router.get("/:id", verifyTokenAndAdmin, getUser);

export default router;
