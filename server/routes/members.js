import express from "express";
import {
  addMember,
  updateMember,
  removeMember,
  getMember,
  getMembers,
} from "../controllers/members.js";
import {
  verifyToken,
  verifyTokenAndAdmin,
} from "../controllers/verifyToken.js";

const router = express.Router();

router.post("/add", verifyToken, addMember);
router.patch("/:id", verifyToken, updateMember);
router.delete("/:id", verifyToken, removeMember);
router.get("/:id", verifyToken, getMember);
router.get("/", verifyTokenAndAdmin, getMembers);

export default router;
