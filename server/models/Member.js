import mongoose from "mongoose";
const MemberSchema = new mongoose.Schema(
  {
    profile: {
      firstName: {
        type: String,
        required: true,
      },
      middleName: {
        type: String,
      },
      lastName: {
        type: String,
        required: true,
      },
      birthday: {
        type: Date,
      },
      contactNumber: {
        type: Number,
      },
      emailAddress: {
        type: String,
      },
      emergencyContactPerson: {
        type: String,
      },
      emergencyContactNumber: {
        type: Number,
      },
      educationalAttainment: {
        type: String,
      },
      knownLanguage: [
        {
          type: String,
        },
      ],
    },
    membership: {
      gospelEncounter: {
        type: String,
      },
      followUpSession1: {
        type: Boolean,
        default: false,
      },
      followUpSession2: {
        type: Boolean,
        default: false,
      },
      followUpSession3: {
        type: Boolean,
        default: false,
      },
      followUpSession4: {
        type: Boolean,
        default: false,
      },
      followUpSession5: {
        type: Boolean,
        default: false,
      },
      followUpSession6: {
        type: Boolean,
        default: false,
      },
      lifeGear101: {
        type: Boolean,
        default: false,
      },
      lifeGear201: {
        type: Boolean,
        default: false,
      },
      lifeGear301: {
        type: Boolean,
        default: false,
      },
      lifeGear401: {
        type: Boolean,
        default: false,
      },
      lifeGear501: {
        type: Boolean,
        default: false,
      },
      waterBaptism: {
        type: Boolean,
        default: false,
      },
      meansOfAttendingChurch: {
        type: String,
      },
    },
    maturity: {
      discipler: {
        type: String,
      },
      disciple: [
        {
          type: String,
        },
      ],
    },
    ministry: {
      primary: {
        type: String,
      },
      secondary: {
        type: String,
      },
      spiritualGifts: [{ type: String }],
      heart: [{ type: String }],
      abilitiese: [
        {
          type: String,
        },
      ],
      personality: [
        {
          type: String,
        },
      ],
      educationalExperience: [{ type: String }],
      workExperience: [{ type: String }],
    },
    mission: {
      missionExposure: [
        {
          type: String,
        },
      ],
      missionaryPartner: {
        type: Boolean,
        default: false,
      },
      currentLocation: {
        type: String,
      },
      trainingsUnderMission: [{ type: String }],
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model("Member", MemberSchema);
